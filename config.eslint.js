module.exports = {
  'env': {
    'browser': false,
    'es6': true
  },
	'extends': [
    'prettier',
    'eslint:recommended',
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
	],
	'globals': {
		'Atomics': 'readonly',
		'SharedArrayBuffer': 'readonly'
	},
	'parser': '@typescript-eslint/parser',
	'parserOptions': {
		'ecmaFeatures': {
			'jsx': true
		},
		'ecmaVersion': 2018,
		'sourceType': 'module'
	},
	'plugins': [
		'@typescript-eslint',
		'prettier'
	],
	'settings': {
		'import/resolver': {
			'typescript': {
				'alwaysTryTypes': true
			}
		},
	},
	'rules': {
    'no-unused-vars': 'off',
    'no-console': 'warn',
    '@typescript-eslint/no-unused-vars': 'error',
		'prettier/prettier': ['error', {
			'singleQuote': true,
			'trailingComma': 'es5',
			'bracketSpacing': true,
			'jsxBracketSameLine': false,
		}],
	}
}
