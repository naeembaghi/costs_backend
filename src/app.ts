import jwt from 'jsonwebtoken';
import path from 'path';
import bodyParser from 'body-parser';
import { GraphQLServer } from 'graphql-yoga';
import dotenv from 'dotenv';
dotenv.config({ path: path.resolve(__dirname, '../.env') });
import accessEnv from './helpers/accessEnv';
const cors = require('cors');
import resolvers from './graphql/resolvers';
import typeDefs from './graphql/typeDefs';
import db from './db/database';

const PORT = accessEnv('PORT', 7000);

const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context: ({ request }) => {
    return { userId: getUserId(request) };
  },
});

const options = {
  port: PORT,
  formatError: (error: any) => {
    console.log(error);
    return error;
  },
};

server.express.use(
  cors({
    origin: (origin: any, cb: any) => cb(null, true),
    credentials: true,
    preflightContinue: true,
    exposedHeaders: [
      'Access-Control-Allow-Headers',
      'Access-Control-Allow-Origin, Origin, X-Requested-With, Content-Type, Accept',
      'X-Password-Expired',
    ],
    optionsSuccessStatus: 200,
  })
);

const getUserId = (req: any) => {
  const { authorization } = req.headers;
  if (authorization) {
    const token = authorization.replace('Bearer ', '');
    const userId = jwt.verify(token, accessEnv('APP_SECRET'));
    return userId;
  }
};

server.express.use(bodyParser.json());

server.express.use((req: any, res: any, next: any) => {
  if (
    typeof req.body?.operationName === 'undefined' ||
    req.body?.operationName === null ||
    ['LOGIN', 'IntrospectionQuery', 'REGISTER'].indexOf(
      req.body.operationName
    ) > -1
  ) {
    next();
    return;
  }
  try {
    const userId = getUserId(req);
    if (userId) {
      next();
    } else {
      throw new Error('Not authenticated');
    }
  } catch {
    throw new Error('Not authenticated');
  }
});

server.start(options, () =>
  console.log(`Graphql server is running on http://localhost:${PORT}`)
);

db.authenticate().then(() => console.log('DB Connected'));
