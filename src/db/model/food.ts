import { Column, Table, DataType, Model } from 'sequelize-typescript';

@Table({
  tableName: 'food',
})
export class Food extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Food;
