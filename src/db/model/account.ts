import {
  Column,
  Table,
  DataType,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import AccountType from './accountType';
import Currency from './currency';
import Entity from './entity';

@Table({
  tableName: 'account',
})
export class Account extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id?: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  name!: string;

  @Column({
    type: DataType.STRING,
    field: 'account_no',
  })
  accountNo: string;

  @Column({
    type: DataType.STRING,
    field: 'card_no',
  })
  cardNo: string;

  @Column({
    type: DataType.FLOAT,
    field: 'start_amount',
  })
  startAmount?: number;

  @Column({
    type: DataType.STRING,
  })
  description?: string;

  @ForeignKey(() => AccountType)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  type_id!: string;

  @BelongsTo(() => AccountType)
  accountType?: AccountType;

  @ForeignKey(() => Currency)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  currency_id!: string;

  @BelongsTo(() => Currency)
  currency?: Currency;

  @ForeignKey(() => Entity)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  owner_id!: string;

  @BelongsTo(() => Entity)
  owner?: Entity;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Account;
