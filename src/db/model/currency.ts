import { Column, Table, DataType, Model, HasMany } from 'sequelize-typescript';
import Account from './account';

@Table({
  tableName: 'currency',
})
export class Currency extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  name!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  symbol!: string;

  @HasMany(() => Account, 'currency_id')
  accounts!: Account[];

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Currency;
