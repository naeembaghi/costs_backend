import { Column, Table, DataType, Model } from 'sequelize-typescript';

@Table({
  tableName: 'user',
})
export class User extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  username!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  password!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  email!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
    field: 'first_name',
  })
  firstName!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
    field: 'last_name',
  })
  lastName!: string;
}

export default User;
