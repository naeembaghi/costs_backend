import { Column, Table, DataType, Model } from 'sequelize-typescript';

@Table({
  tableName: 'meal',
})
export class Meal extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Meal;
