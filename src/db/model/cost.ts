import {
  Column,
  Table,
  DataType,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Type from './type';
import Account from './account';
import Entity from './entity';

@Table({
  tableName: 'costs',
})
export class Cost extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.DATE,
  })
  date!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @Column({
    allowNull: false,
    type: DataType.NUMBER,
  })
  cost!: string;

  @Column({
    allowNull: false,
    type: DataType.NUMBER,
  })
  count!: string;

  @Column({
    allowNull: true,
    type: DataType.STRING,
  })
  description!: string;

  @ForeignKey(() => Type)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  type_id!: string;

  @BelongsTo(() => Type)
  type!: Type;

  @ForeignKey(() => Entity)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  entity_id!: string;

  @BelongsTo(() => Entity)
  entity!: Entity;

  @ForeignKey(() => Entity)
  @Column({
    allowNull: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  entity2_id!: string;

  @BelongsTo(() => Entity)
  secondEntity!: Entity;

  @ForeignKey(() => Account)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  account_id!: string;

  @BelongsTo(() => Account)
  account!: Account;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Cost;
