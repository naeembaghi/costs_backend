import { Column, Table, DataType, Model, HasMany } from 'sequelize-typescript';
import Account from './account';

@Table({
  tableName: 'account_type',
})
export class AccountType extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  name!: string;

  @HasMany(() => Account, 'type_id')
  accounts!: Account[];

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default AccountType;
