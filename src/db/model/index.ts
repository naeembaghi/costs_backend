import Account from './account';
import AccountType from './accountType';
import Currency from './currency';
import Entity from './entity';
import User from './user';
import Food from './food';
import Meal from './meal';
import DailyMeal from './dailyMeal';
import Type from './type';
import Cost from './cost';
import DebtsView from './debtsView';
import Debts from './debts';
import Payments from './payments';

export default [
  Account,
  AccountType,
  Currency,
  Entity,
  User,
  Food,
  Meal,
  DailyMeal,
  Type,
  Cost,
  DebtsView,
  Debts,
  Payments,
];
