import { Column, Table, DataType, Model } from 'sequelize-typescript';

@Table({
  tableName: 'debts_view',
  timestamps: false,
})
export class DebtsView extends Model<DebtsView> {
  @Column({
    allowNull: false,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @Column({
    allowNull: false,
    type: DataType.FLOAT,
  })
  debt!: number;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default DebtsView;
