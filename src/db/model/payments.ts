import {
  Column,
  Table,
  DataType,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Account from './account';
import Entity from './entity';

@Table({
  tableName: 'payment',
})
export class Payment extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.DATE,
  })
  date!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @Column({
    allowNull: false,
    type: DataType.FLOAT,
  })
  amount!: string;

  @ForeignKey(() => Account)
  @Column({
    allowNull: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  account_id!: string;

  @BelongsTo(() => Account)
  account!: Account;

  @ForeignKey(() => Entity)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  person_id!: string;

  @BelongsTo(() => Entity)
  entity!: Entity;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Payment;
