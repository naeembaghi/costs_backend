import { Column, Table, DataType, Model, HasMany } from 'sequelize-typescript';
import Account from './account';

@Table({
  tableName: 'entity',
})
export class Entity extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @HasMany(() => Account, 'owner_id')
  accounts!: Account[];

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Entity;
