import {
  Column,
  Table,
  DataType,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Meal from './meal';
import Food from './food';

@Table({
  tableName: 'daily_meal',
})
export class DailyMeal extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.DATE,
  })
  date!: string;

  @Column({
    allowNull: true,
    type: DataType.DATE,
  })
  description!: string;

  @ForeignKey(() => Meal)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  meal_id!: string;

  @BelongsTo(() => Meal)
  meal!: Meal;

  @ForeignKey(() => Food)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  food_id!: string;

  @BelongsTo(() => Food)
  food!: Food;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default DailyMeal;
