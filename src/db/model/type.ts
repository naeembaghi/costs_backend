import {
  Column,
  Table,
  DataType,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';

@Table({
  tableName: 'types',
})
export class Type extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @ForeignKey(() => Type)
  @Column({
    allowNull: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  parent_type_id!: string;

  @BelongsTo(() => Type)
  parent!: Type;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Type;
