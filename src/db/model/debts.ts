import {
  Column,
  Table,
  DataType,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Account from './account';
import Entity from './entity';

@Table({
  tableName: 'debts',
})
export class Debt extends Model {
  @Column({
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  id!: string;

  @Column({
    allowNull: false,
    type: DataType.DATE,
  })
  date!: string;

  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  title!: string;

  @Column({
    allowNull: true,
    type: DataType.STRING,
  })
  description!: string;

  @Column({
    allowNull: false,
    type: DataType.FLOAT,
  })
  cost!: string;

  @Column({
    allowNull: false,
    type: DataType.FLOAT,
  })
  count!: string;

  @ForeignKey(() => Account)
  @Column({
    allowNull: true,
    type: DataType.INTEGER.UNSIGNED,
  })
  account_id!: string;

  @BelongsTo(() => Account)
  account!: Account;

  @ForeignKey(() => Entity)
  @Column({
    allowNull: false,
    type: DataType.INTEGER.UNSIGNED,
  })
  entity_id!: string;

  @BelongsTo(() => Entity)
  entity!: Entity;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  user_id!: number;
}

export default Debt;
