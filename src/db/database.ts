import { Sequelize } from 'sequelize-typescript';
import accessEnv from '../helpers/accessEnv';
import models from './model';

const DB_URL = accessEnv('DB_URL');

const db = new Sequelize(DB_URL, {
  dialectOptions: {
    charset: 'utf8',
    multipleStatements: true,
  },
  logging: false,
  dialect: 'mysql',
  define: {
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  },
  models,
});

(global as any).sequelize = db;

export default db;
