import AccountType from './AccountType';
import Food from './Food';
import Currency from './Currency';
import Account from './Account';
import Entity from './Entity';
import Meal from './Meal';
import DailyMeal from './DailyMeal';
import Type from './Type';
import Cost from './Cost';
import Payment from './Payment';
import Debt from './Debt';
import User from './User';
import Query from './query';
import * as Mutation from './mutation';
import GraphQLJSON, { GraphQLJSONObject } from 'graphql-type-json';

const resolvers = {
  Food,
  AccountType,
  Currency,
  Account,
  Entity,
  Meal,
  DailyMeal,
  Type,
  Cost,
  Payment,
  Debt,
  User,
  Mutation,
  Query,
  JSON: GraphQLJSON,
  JSONObject: GraphQLJSONObject,
};

export default resolvers;
