import Account from '../../db/model/account';
import AccountType from '../../db/model/accountType';
import Currency from '../../db/model/currency';
import Entity from '../../db/model/entity';

const resolvers = {
  accountType: (account: Account) => {
    return AccountType.findOne({
      where: { id: account.type_id },
    });
  },
  currency: (account: Account) => {
    return Currency.findOne({
      where: { id: account.currency_id },
    });
  },
  owner: (account: Account) => {
    return Entity.findOne({
      where: { id: account.owner_id },
    });
  },
};

export default resolvers;
