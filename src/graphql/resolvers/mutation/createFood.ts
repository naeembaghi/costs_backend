import Food from '../../../db/model/food';

const createFoodResolver = (
  _: any,
  { foodInput: { title } }: { foodInput: { title: string } },
  context: any
) => {
  return Food.create({ title, user_id: context.userId.userId });
};

export default createFoodResolver;
