import DailyMeal from '../../../db/model/dailyMeal';

const createDailyMealResolver = (
  _: any,
  {
    dailyMealInput: { date, food, meal },
  }: {
    dailyMealInput: {
      date: string;
      food: { id: string };
      meal: { id: string };
    };
  },
  context: any
) => {
  return DailyMeal.create({
    date,
    food_id: food.id,
    meal_id: meal.id,
    user_id: context.userId.userId,
  });
};

export default createDailyMealResolver;
