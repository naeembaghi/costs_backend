import Currency from '../../../db/model/currency';

const createCurrencyResolver = (
  context: any,
  {
    currencyInput: { name, symbol },
  }: { currencyInput: { name: string; symbol: string } }
) => {
  return Currency.create({ name, symbol, user_id: context.userId.userId });
};

export default createCurrencyResolver;
