import Type from '../../../db/model/type';

const createTypeResolver = (
  context: any,
  {
    typeInput: { title },
  }: {
    typeInput: {
      title: string;
    };
  }
) => {
  return Type.create({
    title,
  });
};

export default createTypeResolver;
