import Payment from '../../../db/model/payments';

const createPaymentResolver = (
  _: any,
  {
    paymentInput: { title, date, amount, entity, account },
  }: {
    paymentInput: {
      title: string;
      date: string;
      amount: number;
      entity: { id: number };
      account: { id: number };
    };
  },
  context: any
) => {
  return Payment.create({
    date,
    title,
    amount,
    person_id: entity.id,
    account_id: account.id,
    user_id: context.userId.userId,
  });
};

export default createPaymentResolver;
