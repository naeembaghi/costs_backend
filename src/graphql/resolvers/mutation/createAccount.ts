import Account from '../../../db/model/account';

const createAccountResolver = (
  _: any,
  {
    accountInput: { name, cardNo, accountNo, accountType, currency, owner },
  }: {
    accountInput: {
      name: string;
      cardNo: string;
      accountNo: string;
      accountType: { id: string };
      currency: { id: string };
      owner: { id: string };
    };
  },
  context: any
) => {
  return Account.create({
    name,
    cardNo,
    accountNo,
    type_id: accountType.id,
    currency_id: currency.id,
    owner_id: owner.id,
    user_id: context.userId.userId,
  });
};

export default createAccountResolver;
