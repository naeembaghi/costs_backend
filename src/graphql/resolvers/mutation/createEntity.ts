import Entity from '../../../db/model/entity';

const createEntityResolver = (
  _: any,
  { entityInput: { title } }: { entityInput: { title: string } },
  context: any
) => {
  return Entity.create({ title, user_id: context.userId.userId });
};

export default createEntityResolver;
