import Meal from '../../../db/model/meal';

const createMealResolver = (
  _: any,
  { mealInput: { title } }: { mealInput: { title: string } },
  context: any
) => {
  return Meal.create({ title, user_id: context.userId.userId });
};

export default createMealResolver;
