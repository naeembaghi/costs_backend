import AccountType from '../../../db/model/accountType';

const createAccountTypeResolver = (
  _: any,
  { accountTypeInput: { name } }: { accountTypeInput: { name: string } },
  context: any
) => {
  return AccountType.create({ name, user_id: context.userId.userId });
};

export default createAccountTypeResolver;
