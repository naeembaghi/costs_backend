import User from '../../../db/model/user';
import bcrypt from 'bcrypt';
import accessEnv from '../../../helpers/accessEnv';
import jwt from 'jsonwebtoken';

export const registerResolver = async (
  context: any,
  {
    userInput: { username, password, email, firstName, lastName },
  }: {
    userInput: {
      username: string;
      password: string;
      email: string;
      firstName: string;
      lastName: string;
    };
  }
) => {
  const hashedPassword = await bcrypt.hash(password, 10);
  const user = await User.create({
    username,
    password: hashedPassword,
    email,
    firstName,
    lastName,
  });
  const token = jwt.sign({ userId: user.id }, accessEnv('APP_SECRET'));

  return { user, token };
};

export const loginResolver = async (
  context: any,
  { username, password }: { username: string; password: string }
) => {
  const user = await User.findOne({
    where: { username },
  });
  if (!user) throw new Error('Invalid username or password');

  const isValid = bcrypt.compare(password, user.password);
  if (!isValid) throw new Error('Invalid username or password');

  const token = jwt.sign({ userId: user.id }, accessEnv('APP_SECRET'));
  return { user, token };
};
