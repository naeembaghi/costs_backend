import Cost from '../../../db/model/cost';

const createCostResolver = (
  _: any,
  {
    costInput: {
      date,
      title,
      type,
      cost,
      count,
      description,
      entity,
      secondEntity,
      account,
    },
  }: {
    costInput: {
      date: string;
      title: string;
      type: { id: string };
      cost: number;
      count: number;
      description: string;
      entity: { id: string };
      secondEntity: { id: string };
      account: { id: string };
    };
  },
  context: any
) => {
  return Cost.create({
    date,
    title,
    description,
    count,
    cost,
    entity_id: entity.id,
    entity2_id: secondEntity?.id,
    account_id: account.id,
    type_id: type.id,
    user_id: context.userId.userId,
  });
};

export default createCostResolver;
