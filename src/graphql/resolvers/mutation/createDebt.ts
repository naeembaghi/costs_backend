import Debt from '../../../db/model/debts';

const createDebtResolver = (
  _: any,
  {
    debtInput: { title, date, cost, count, entity, account, description },
  }: {
    debtInput: {
      title: string;
      date: string;
      cost: number;
      count: number;
      entity: { id: number };
      account: { id: number };
      description: string;
    };
  },
  context: any
) => {
  return Debt.create({
    date,
    title,
    cost,
    count,
    entity_id: entity.id,
    account_id: account.id,
    description,
    user_id: context.userId.userId,
  });
};

export default createDebtResolver;
