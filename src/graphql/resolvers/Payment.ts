import Payment from '../../db/model/payments';
import Account from '../../db/model/account';
import Entity from '../../db/model/entity';

const resolvers = {
  account: (payment: Payment) => {
    return Account.findOne({
      where: { id: payment.account_id },
    });
  },
  entity: (payment: Payment) => {
    return Entity.findOne({
      where: { id: payment.person_id },
    });
  },
};

export default resolvers;
