import Debt from '../../db/model/debts';
import Account from '../../db/model/account';
import Entity from '../../db/model/entity';

const resolvers = {
  account: (debt: Debt) => {
    return Account.findOne({
      where: { id: debt.account_id },
    });
  },
  entity: (debt: Debt) => {
    return Entity.findOne({
      where: { id: debt.entity_id },
    });
  },
};

export default resolvers;
