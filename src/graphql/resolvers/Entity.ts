import Account from '../../db/model/account';
import Entity from '../../db/model/entity';

const resolvers = {
  accounts: (entity: Entity) => {
    return Account.findAll({
      include: [
        {
          model: Entity,
          where: { id: entity.id },
        },
      ],
      order: [['name', 'ASC']],
    });
  },
};

export default resolvers;
