import Account from '../../db/model/account';
import Currency from '../../db/model/currency';

const resolvers = {
  accounts: (currency: Currency) => {
    return Account.findAll({
      include: [
        {
          model: Currency,
          where: { id: currency.id },
        },
      ],
      order: [['name', 'ASC']],
    });
  },
};

export default resolvers;
