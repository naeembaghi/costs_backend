import Cost from '../../db/model/cost';
import Entity from '../../db/model/entity';
import Account from '../../db/model/account';
import Type from '../../db/model/type';

const resolvers = {
  account: (cost: Cost) => {
    return Account.findOne({
      where: { id: cost.account_id },
    });
  },
  type: (cost: Cost) => {
    return Type.findOne({
      where: { id: cost.type_id },
    });
  },
  entity: (cost: Cost) => {
    return Entity.findOne({
      where: { id: cost.entity_id },
    });
  },
  secondEntity: (cost: Cost) => {
    return Entity.findOne({
      where: { id: cost.entity2_id },
    });
  },
};

export default resolvers;
