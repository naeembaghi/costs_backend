import Debt from '../../../db/model/debts';
import type { Args } from './typeDefs';

const debtResolver = (_: any, { order, where, limit, offset }: Args) => {
  return Debt.findAll({ where, limit, order, offset });
};

export default debtResolver;
