import User from '../../../db/model/user';
import type { Args } from './typeDefs';

const userResolver = (_: any, { order, where, limit, offset }: Args) => {
  return User.findAll({ where, limit, order, offset });
};

export default userResolver;
