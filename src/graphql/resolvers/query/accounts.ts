import Account from '../../../db/model/account';
import type { Args } from './typeDefs';

const accountsResolver = (_: any, { where, limit, offset, order }: Args) => {
  return Account.findAll({ where, limit, offset, order });
};

export default accountsResolver;
