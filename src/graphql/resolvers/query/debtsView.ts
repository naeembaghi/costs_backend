import DebtsView from '../../../db/model/debtsView';
import type { Args } from './typeDefs';

const debtsViewResolver = (_: any, { order, where, limit, offset }: Args) => {
  return DebtsView.findAll({ where, limit, order, offset });
};

export default debtsViewResolver;
