import Type from '../../../db/model/type';
import type { Args } from './typeDefs';

const typeResolver = (_: any, { order, where, limit, offset }: Args) => {
  return Type.findAll({ where, limit, order, offset });
};

export default typeResolver;
