import Food from '../../../db/model/food';
import type { Args } from './typeDefs';

const foodResolver = (_: any, { order, where, limit, offset }: Args) => {
  return Food.findAll({ where, limit, order, offset });
};

export default foodResolver;
