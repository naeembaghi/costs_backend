import accounts from './accounts';
import accountTypes from './accountTypes';
import currencies from './currencies';
import entities from './entities';
import foods from './foods';
import meals from './meals';
import dailyMeals from './dailyMeals';
import types from './types';
import costs from './costs';
import debtsView from './debtsView';
import debts from './debts';
import payments from './payments';
import costsReport from './costsReport';
import costsTypeReport from './costsTypeReport';
export { default as users } from './users';
import type { Args } from './typeDefs';

const queryResolvers = {
  accounts,
  accountTypes,
  currencies,
  entities,
  foods,
  meals,
  dailyMeals,
  types,
  costs,
  debtsView,
  debts,
  payments,
  costsReport,
  costsTypeReport,
};

export default Object.keys(queryResolvers).reduce((total, each) => {
  return {
    ...total,
    [each]: (
      _: any,
      { order, where, limit, offset, ...others }: Args,
      context: any
    ) => {
      // @ts-ignore
      const resolver = queryResolvers[each];
      return resolver(
        _,
        {
          where: { user_id: context.userId.userId, ...where },
          limit,
          offset,
          order,
          ...others,
        },
        context
      );
    },
  };
}, {});
