import Cost from '../../../db/model/cost';
import type { Args } from './typeDefs';

const costResolver = (_: any, { limit, offset, order, where }: Args) => {
  return Cost.findAll({
    limit,
    offset,
    order,
    where,
  });
};

export default costResolver;
