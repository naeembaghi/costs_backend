import Meal from '../../../db/model/meal';
import type { Args } from './typeDefs';

const mealResolver = (_: any, { order, where, limit, offset }: Args) => {
  return Meal.findAll({ where, limit, order, offset });
};

export default mealResolver;
