import AccountType from '../../../db/model/accountType';
import type { Args } from './typeDefs';

const accountTypesResolver = (
  _: any,
  { order, where, limit, offset }: Args
) => {
  return AccountType.findAll({ where, limit, order, offset });
};

export default accountTypesResolver;
