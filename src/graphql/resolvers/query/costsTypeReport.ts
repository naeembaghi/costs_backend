import { QueryTypes, Sequelize } from 'sequelize';

const costsReportResolver = async (
  _: any,
  { firstDate, secondDate, where: { user_id } }: any
) => {
  const query = `call costs_calculator_by_type_date('${firstDate}', '${secondDate}', ${user_id});`;
  const res = await ((global as any).sequelize as Sequelize).query(query, {
    type: QueryTypes.SELECT,
  });
  return Object.values(res[0]).map((each) => each ?? 0);
};

export default costsReportResolver;
