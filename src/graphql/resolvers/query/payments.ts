import Payment from '../../../db/model/payments';
import type { Args } from './typeDefs';

const paymentResolver = (_: any, { order, where, limit, offset }: Args) => {
  return Payment.findAll({ where, limit, order, offset });
};

export default paymentResolver;
