import { QueryTypes, Sequelize } from 'sequelize';

const costsReportResolver = async (
  _: any,
  { firstDates, secondDates, where: { user_id } }: any
) => {
  let query = 'select ';
  firstDates.forEach((firstDate: string, idx: number) => {
    query += `costs_calculator_by_date_limits("${firstDate}", "${secondDates[idx]}", ${user_id}), `;
  });
  query = query.substring(0, query.length - 2);
  query += ';';
  const res = await ((global as any).sequelize as Sequelize).query(query, {
    type: QueryTypes.SELECT,
  });
  return Object.values(res[0]).map((each) => each ?? 0);
};

export default costsReportResolver;
