import Currency from '../../../db/model/currency';
import type { Args } from './typeDefs';

const currencyResolver = (_: any, { order, where, limit, offset }: Args) => {
  return Currency.findAll({ where, limit, order, offset });
};

export default currencyResolver;
