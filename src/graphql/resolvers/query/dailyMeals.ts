import DailyMeal from '../../../db/model/dailyMeal';
import type { Args } from './typeDefs';

const dailyMealResolver = (_: any, { order, where, limit, offset }: Args) => {
  return DailyMeal.findAll({ where, limit, order, offset });
};

export default dailyMealResolver;
