export type Args = {
  where: any;
  offset: number;
  limit: number;
  order: Array<[string, 'ASC' | 'DESC']>;
};
