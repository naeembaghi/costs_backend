import Entity from '../../../db/model/entity';
import type { Args } from './typeDefs';

const entityResolver = (_: any, { order, where, limit, offset }: Args) => {
  return Entity.findAll({ where, limit, order, offset });
};

export default entityResolver;
