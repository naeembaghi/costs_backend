import DailyMeal from '../../db/model/dailyMeal';
import Food from '../../db/model/food';
import Meal from '../../db/model/meal';

const resolvers = {
  meal: (dailyMeal: DailyMeal) => {
    return Meal.findOne({
      where: { id: dailyMeal.meal_id },
    });
  },
  food: (dailyMeal: DailyMeal) => {
    return Food.findOne({
      where: { id: dailyMeal.food_id },
    });
  },
};

export default resolvers;
