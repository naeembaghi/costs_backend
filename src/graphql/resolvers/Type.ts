import Type from '../../db/model/type';

const resolvers = {
  parent: (type: Type) => {
    return Type.findOne({
      where: { id: type.parent_type_id },
    });
  },
};

export default resolvers;
