import Account from '../../db/model/account';
import AccountType from '../../db/model/accountType';

const resolvers = {
  accounts: (accountType: AccountType) => {
    return Account.findAll({
      include: [
        {
          model: AccountType,
          where: { id: accountType.id },
        },
      ],
      order: [['name', 'ASC']],
    });
  },
};

export default resolvers;
