const Meal = `
  input MealInput {
    title: String!
  }
  type Meal {
    id: ID!
    title: String!
  }
  extend type Query {
    meals(where: JSON, order: JSON, limit: Int, offset: Int): [Meal!]!
  }
  extend type Mutation {
    createMeal(mealInput: MealInput!): Meal!
  }
`;

export default Meal;
