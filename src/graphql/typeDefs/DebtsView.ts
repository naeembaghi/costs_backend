const DebtsView = `
  type DebtsView {
    id: ID
    title: String
    debt: Float
  }
  extend type Query {
    debtsView(where: JSON, order: JSON, limit: Int, offset: Int): [DebtsView!]!
  }
`;

export default DebtsView;
