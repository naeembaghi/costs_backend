const Account = `
  input AccountInput {
    name: String!
    accountType: InputEntity!
    currency: InputEntity!
    owner: InputEntity!
    accountNo: String
    cardNo: String
    startAmount: Float
    description: String
  }
  type Account {
    id: ID!
    name: String!
    currency_id: String!
    type_id: String!
    accountType: AccountType!
    owner_id: String!
    owner: Entity!
    accountNo: String
    cardNo: String
    startAmount: Float
    description: String
    currency: Currency!
  }
  type Query {
    accounts(where: JSON, order: JSON, limit: Int, offset: Int): [Account!]!
  }
  type Mutation {
    createAccount(accountInput: AccountInput!): Account!
  }
`;

export default Account;
