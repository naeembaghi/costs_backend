const Entity = `
  input EntityInput {
    title: String!
  }
  type Entity {
    id: ID!
    title: String!
    accounts: [Account!]!
  }
  extend type Query {
    entities(where: JSON, order: JSON, limit: Int, offset: Int): [Entity!]!
  }
  extend type Mutation {
    createEntity(entityInput: EntityInput!): Entity!
  }
`;

export default Entity;
