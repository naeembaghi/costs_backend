const Food = `
  input FoodInput {
    title: String!
  }
  type Food {
    id: ID!
    title: String!
  }
  extend type Query {
    foods(where: JSON, order: JSON, limit: Int, offset: Int): [Food!]!
  }
  extend type Mutation {
    createFood(foodInput: FoodInput!): Food!
  }
`;

export default Food;
