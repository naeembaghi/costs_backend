const DailyMeal = `
  input DailyMealInput {
    date: String!
    meal: InputEntity!
    food: InputEntity!
    description: String
  }
  type DailyMeal {
    id: ID!
    date: String!
    meal: Meal!
    food: Food!
    description: String
  }
  extend type Query {
    dailyMeals(where: JSON, order: JSON, limit: Int, offset: Int): [DailyMeal!]!
  }
  extend type Mutation {
    createDailyMeal(dailyMealInput: DailyMealInput!): DailyMeal!
  }
`;

export default DailyMeal;
