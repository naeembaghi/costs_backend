import Account from './Account';
import AccountType from './AccountType';
import Currency from './Currency';
import Entity from './Entity';
import User from './User';
import Food from './Food';
import Meal from './Meal';
import DailyMeal from './DailyMeal';
import Type from './Type';
import Cost from './Cost';
import DebtsView from './DebtsView';
import Debts from './Debts';
import Payments from './Payments';
import CostsReport from './CostsReport';
import CostsTypeReport from './CostsTypeReport';

const JSONScalar = `
  scalar JSON
  scalar JSONObject
`;

const InputEntity = `
  input InputEntity {
    id: ID!
  }
`;

export default [
  JSONScalar,
  InputEntity,
  Account,
  AccountType,
  Currency,
  Entity,
  User,
  Food,
  Meal,
  DailyMeal,
  Type,
  Cost,
  DebtsView,
  Debts,
  Payments,
  CostsReport,
  CostsTypeReport,
];
