const CostsTypeReport = `
  type CostsTypeReport {
    title: String
    sum: Float
  }
  
  extend type Query {
    costsTypeReport(firstDate: String, secondDate: String): [CostsTypeReport!]!
  }
`;

export default CostsTypeReport;
