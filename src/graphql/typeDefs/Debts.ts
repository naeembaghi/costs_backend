const Debts = `
  input DebtInput {
    date: String!
    title: String!
    cost: Float!
    count: Float!
    entity: InputEntity!
    account: InputEntity!
    description: String
  }
  type Debt {
    id: ID!
    date: String!
    title: String!
    cost: Float!
    count: Float!
    entity: Entity!
    account: Account
    description: String
  }
  extend type Query {
    debts(where: JSON, order: JSON, limit: Int, offset: Int): [Debt!]!
  }
  extend type Mutation {
    createDebt(debtInput: DebtInput!): Debt!
  }
`;

export default Debts;
