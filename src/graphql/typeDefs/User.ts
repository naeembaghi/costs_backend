const User = `
  input UserInput {
    username: String!
    password: String!
    email: String!
    firstName: String
    lastName: String
  }
  type User {
    id: ID!
    username: String!
    email: String!
    firstName: String
    lastName: String
  }
  extend type Query {
    users(where: JSON, order: JSON, limit: Int, offset: Int): [User!]!
  }
  type AuthPayload {
    token: String
    user: User
  }
  extend type Mutation {
    login(username: String!, password: String!): AuthPayload
    register(userInput: UserInput): AuthPayload
  }
`;

export default User;
