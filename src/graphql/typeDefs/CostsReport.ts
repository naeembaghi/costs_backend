const CostsReport = `
  extend type Query {
    costsReport(firstDates: [String], secondDates: [String]): [Float!]!
  }
`;

export default CostsReport;
