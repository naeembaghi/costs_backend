const Type = `
  input TypeInput {
    title: String!
  }
  type Type {
    id: ID!
    title: String!
    parent: Type
  }
  extend type Query {
    types(where: JSON, order: JSON, limit: Int, offset: Int): [Type!]!
  }
  extend type Mutation {
    createType(typeInput: TypeInput!): Type!
  }
`;

export default Type;
