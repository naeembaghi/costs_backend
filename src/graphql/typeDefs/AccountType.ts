const AccountType = `
  input AccountTypeInput {
    name: String!
  }
  type AccountType {
    id: ID!
    name: String!
    accounts: [Account!]!
  }
  extend type Query {
    accountTypes(where: JSON, order: JSON, limit: Int, offset: Int): [AccountType!]!
  }
  extend type Mutation {
    createAccountType(accountTypeInput: AccountTypeInput!): AccountType!
  }
`;

export default AccountType;
