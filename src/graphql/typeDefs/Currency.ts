const Currency = `
  input CurrencyInput {
    name: String!
    symbol: String
  }
  type Currency {
    id: ID!
    name: String!
    symbol: String
    accounts: [Account]
  }
  extend type Query {
    currencies(where: JSON, order: JSON, limit: Int, offset: Int): [Currency!]!
  }
  extend type Mutation {
    createCurrency(currencyInput: CurrencyInput!): Currency!
  }
`;

export default Currency;
