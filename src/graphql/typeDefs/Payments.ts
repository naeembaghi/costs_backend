const Payments = `
  input PaymentInput {
    date: String!
    title: String!
    amount: Float!
    entity: InputEntity!
    account: InputEntity!
    description: String
  }
  type Payment {
    id: ID!
    date: String!
    title: String!
    amount: Float!
    entity: Entity!
    account: Account
    description: String
  }
  extend type Query {
    payments(where: JSON, order: JSON, limit: Int, offset: Int): [Payment!]!
  }
  extend type Mutation {
    createPayment(paymentInput: PaymentInput!): Payment!
  }
`;

export default Payments;
