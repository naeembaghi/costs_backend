const Cost = `
  input CostInput {
    date: String!
    title: String!
    type: InputEntity!
    cost: Float!
    count: Float!
    entity: InputEntity!
    secondEntity: InputEntity
    description: String
    account: InputEntity!
  }
  type Cost {
    id: ID!
    date: String!
    title: String!
    type: Type!
    cost: Float!
    count: Float!
    entity: Entity!
    secondEntity: Entity
    description: String
    account: Account!
  }
  extend type Query {
    costs(where: JSON, order: JSON, limit: Int, offset: Int): [Cost!]!
  }
  extend type Mutation {
    createCost(costInput: CostInput!): Cost!
  }
`;

export default Cost;
